package edu.purdue.cs.cs180.server;

import edu.purdue.cs.cs180.channel.Channel;
import edu.purdue.cs.cs180.channel.ChannelException;
import edu.purdue.cs.cs180.channel.MessageListener;
import edu.purdue.cs.cs180.channel.TCPChannel;
import edu.purdue.cs.cs180.common.Message;
import edu.purdue.cs.cs180.server.Data;

public class Matcher extends Thread	{
	
	private long sleepTime;
	private DataFeeder feeder;
    private String matchingType;
	private Channel channel;
	public Matcher(DataFeeder feeder, long sleep, String matchingType, Channel channel) {
		this.feeder = feeder;
		sleepTime = sleep;
		this.matchingType = matchingType;
		this.channel = channel;
	}
	
	public void messageSender(Message requester, Message responder){
		try {
	   		channel.sendMessage(new Message(Message.Type.Assigned, requester.getInfo(), requester.getClientID()).toString(), responder.getClientID());
	   		channel.sendMessage(new Message(Message.Type.Assigned, responder.getInfo(), responder.getClientID()).toString(), requester.getClientID());
	   	}catch(ChannelException e) {
			e.printStackTrace();
			System.exit(1);
	   	}
	}

	@Override
	public void run() {
		while(true){

			try {
		      Thread.sleep(sleepTime);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }

		    while(feeder.hasNextRequest() && feeder.hasNextResponse()){

		    	if(matchingType.equals("FCFS")){
		    		Message requester = feeder.getFirstRequest();
		    		Message responder = feeder.getFirstResponse();
		    		messageSender(requester, responder);
				   	feeder.removeFirstRequest();
				   	feeder.removeFirstResponse();
			    }
			  	else{
			  		Message requester = feeder.getLastRequest();
		    		Message responder = feeder.getLastResponse();
		    		messageSender(requester, responder);
				   	feeder.removeLastRequest();
				   	feeder.removeLastResponse();
			  	}

		    }

		}
	}
}