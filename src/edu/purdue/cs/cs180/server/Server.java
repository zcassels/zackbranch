package edu.purdue.cs.cs180.server;

import java.util.Arrays;

import edu.purdue.cs.cs180.channel.Channel;
import edu.purdue.cs.cs180.channel.ChannelException;
import edu.purdue.cs.cs180.channel.MessageListener;
import edu.purdue.cs.cs180.channel.TCPChannel;
import edu.purdue.cs.cs180.common.Message;
import edu.purdue.cs.cs180.server.Data;

public class Server extends Thread implements MessageListener {

	private Channel channel = null;
	private Data d;
	private Matcher m;

	public Server(int port, String matchingType, int sleep) {
		channel = new TCPChannel(port);
		channel.setMessageListener(this);
		d = new Data();
		m = new Matcher(d, sleep, matchingType, channel);
		m.start();
	}

	@Override
	public void messageReceived(String messageString, int clientID) {

		assert (messageString != null);
		System.out.println(clientID + ": " + messageString); // For debugging
		Message message = new Message(messageString, clientID);
		d.addMessage(message);
		try {
			channel.sendMessage(new Message(Message.Type.Searching, "", clientID).toString(), clientID);
		} catch (ChannelException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
	}

	/*
	* @param (int) the server port
	* @param (String) the matchingtype (for the Matcher thread)
	* @param (int) the sleep time (for the Matcher thread)
	*/
	public static void main(String[] args) {		
		new Server(Integer.parseInt(args[0]), args[1], Integer.parseInt(args[2]));
	}
}
