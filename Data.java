import java.util.ArrayList;
import edu.purdue.cs.cs180.channel.Channel;
import edu.purdue.cs.cs180.channel.ChannelException;
	
public class Data implements DataFeeder{
	
	public ArrayList<Message> pendingRequesters = new ArrayList<Message>();
	public ArrayList<Message> pendingResponders = new ArrayList<Message>();


	public void addMessage(Message message){
		assert (message != null);
		
		switch (message.getType()) {
			case Request:
				pendingRequesters.add(message);
				break;
			case Response:
				pendingResponders.add(message);
				break;
			default:
				System.err.println("Unexpected message of type " + message.getType());
				break;
			}
	}

	public Message getFirstRequest(){
		return pendingRequesters.get(pendingRequesters.size()-1);
	}

	public Message getFirstResponse(){
		return pendingResponders.get(pendingResponders.size()-1);
	}

	public Message getLastRequest(){
		return pendingRequesters.get(0);
		
	}

	public Message getLastResponse(){
		return pendingResponders.get(0);
	}

	public boolean hasNextRequest(){
		if(pendingRequesters.size()==0)
			return false;
		else
			return true;
	}

	public boolean hasNextResponse(){
		if(pendingResponders.size()==0)
			return false;
		else
			return true;
	}

	public void removeFirstRequest(){
		pendingRequesters.remove(pendingRequesters.size()-1);
	}

	public void removeFirstResponse(){
		pendingResponders.remove(pendingResponders.size()-1);
	}

	public void removeLastRequest(){
		pendingRequesters.remove(0);
	}

	public void removeLastResponse(){
		pendingResponders.remove(0);
	}
}