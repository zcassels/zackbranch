import java.util.Arrays;

import edu.purdue.cs.cs180.channel.Channel;
import edu.purdue.cs.cs180.channel.ChannelException;
import edu.purdue.cs.cs180.channel.MessageListener;
import edu.purdue.cs.cs180.channel.TCPChannel;

public class Server extends Thread implements MessageListener {

	private Channel channel = null;
	private Data d;
	private Matcher m;
	//private int sleep = 0;

	public Server(int port, String matchingType, int sleep) {
		channel = new TCPChannel(port);
		channel.setMessageListener(this);
		//this.matchType = matchType;
		//this.sleep = sleep;
		d = new Data();
		m = new Matcher(d, sleep, matchingType, channel);
	}

	/*
	private void messageHandler(Message message, ArrayList<Message> from,
			ArrayList<Message> to) {
		assert (message != null);
		assert (from != null);
		assert (to != null);
		if (from.size() > 0) {
			Message responderMessage = from.remove(0);
			try {
				channel.sendMessage(new Message(Message.Type.Assigned, 
						message.getInfo(), responderMessage.getClientID()).toString(),
						responderMessage.getClientID());

				channel.sendMessage(new Message(Message.Type.Assigned,
						responderMessage.getInfo(), message.getClientID()).toString(), 
						message.getClientID());
			} catch (ChannelException e) {
				e.printStackTrace();
				System.exit(1);
			}
		} else {
			to.add(message);
			try {
				channel.sendMessage(new Message(Message.Type.Searching, 
						"", message.getClientID()).toString(),
						message.getClientID());
			} catch (ChannelException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
	}
	*/
	
	@Override
	public void messageReceived(String messageString, int clientID) {

		assert (messageString != null);
		System.out.println(clientID + ": " + messageString); // For debugging
		Message message = new Message(messageString, clientID);
		d.addMessage(message);
		try {
			channel.sendMessage(new Message(Message.Type.Searching, "", clientID).toString(), clientID);
		} catch (ChannelException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
	}

	/*
	* @param (int) the server port
	* @param (String) the matchingtype (for the Matcher thread)
	* @param (int) the sleep time (for the Matcher thread)
	*/
	public static void main(String[] args) {		
		new Server(Integer.parseInt(args[0]), args[1], Integer.parseInt(args[2]));
	}
}
