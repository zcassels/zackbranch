public class Message {

	public enum Type {
		Request,
		Response,
		Searching,
		Assigned
	}

	private Type type;
	private String info;
	private int clientID;

	public Message(Type type, String info, int clientID) {
		this.type = type;
		this.info = info;
		this.clientID = clientID;
	}

	public Message(String messageString, int clientID) {
		String[] messageParts = messageString.split(":", 2);
		this.type = Type.valueOf(messageParts[0]);
		this.info = messageParts[1];
		this.clientID = clientID;
	}

	public Type getType() {
		return type;
	}

	public String getInfo() {
		return info;
	}

	public int getClientID() {
		return clientID;
	}

	public String toString() {
		return this.type.name() + ":" + this.info;
	}
}
